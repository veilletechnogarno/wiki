import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Wiki",
  base:"/wiki/",
  description: "Un recueil des ancients sites étudiants :)",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Accueil', link: '/' },
      { text: 'Site de cours', link: 'https://veilletechnogarno.gitlab.io/docs/' }
    ],

    sidebar: [
      {
        
      }
    ],

  }
})
