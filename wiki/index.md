---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Veille Techno"
  text: "Recueil des anciens projets"
  tagline: 
  actions:
    - theme: brand
      text: A23
      link: /a23

    - theme: brand
      text: H24
      link: /h24

    - theme: brand
      text: A24
      link: /a24
---

